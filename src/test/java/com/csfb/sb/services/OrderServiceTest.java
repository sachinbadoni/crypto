package com.csfb.sb.services;

import com.csfb.sb.domain.Order;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.csfb.sb.domain.OrderType.BUY;
import static com.csfb.sb.domain.OrderType.SELL;
import static java.math.BigDecimal.ONE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class OrderServiceTest {
    private OrderService orderService;

    @Before
    public void init(){
        orderService = new OrderService();
    }

    @Test
    public void it_should_store_order() {
        Order order = defaultOrder();
        Order saveOrder = orderService.saveOrder(order);
        assertNotNull(saveOrder.getId());

    }

    @Test
    public void it_should_be_able_to_fetch_order() {
        Order order = defaultOrder();
        Order saveOrder = orderService.saveOrder(order);
        assertNotNull(saveOrder.getId());

        Optional<Order> optionalOrder = orderService.getOrder(saveOrder.getId());
        assertTrue(optionalOrder.isPresent());
        assertThat(optionalOrder.get(), is(saveOrder));

    }

    @Test
    public void it_should_be_able_to_cancel_order() {
        Order order = defaultOrder();
        Order saveOrder = orderService.saveOrder(order);
        assertNotNull(saveOrder.getId());

        orderService.cancelOrder(saveOrder.getId());

        Optional<Order> optionalOrder = orderService.getOrder(saveOrder.getId());
        assertFalse(optionalOrder.isPresent());
    }


    @Test
    public void it_should_be_able_to_return_all_orders() {
        Order order1 = Order.builder().orderQuantity(ONE).userId("user-1").coinType("(Litecoin").orderType(BUY).build();
        Order order2 = Order.builder().orderQuantity(ONE).userId("user-2").coinType("(Litecoin").orderType(SELL).build();


       orderService.saveOrder(order1);
       orderService.saveOrder(order2);

        List<Order> fetchedOrders =  orderService.getAllOrders();
        assertThat(fetchedOrders, is(Arrays.asList(order1,order2)));
    }


    private Order defaultOrder() {
        return Order.builder()
                .orderQuantity(ONE)
                .userId("user-1")
                .coinType("(Litecoin")
                .orderType(BUY)
                .build();
    }

}