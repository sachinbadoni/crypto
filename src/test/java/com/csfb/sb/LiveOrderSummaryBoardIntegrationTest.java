package com.csfb.sb;

import com.csfb.sb.domain.Order;
import com.csfb.sb.domain.OrderSummary;
import com.csfb.sb.domain.OrderType;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;


public class LiveOrderSummaryBoardIntegrationTest {

    private LiveOrderSummaryBoard liveOrderBoard;

    @Before
    public void init() {
        liveOrderBoard = new LiveOrderSummaryBoard();
    }

    @Test
    public void it_should_save_order() {
        //This tests asserts both saving and then fetching of all records
        Order order = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.BUY).coinType("bit").userId("u1").build();

        liveOrderBoard.addOrder(order);

        assertThat(liveOrderBoard.getOrders(), contains(order));
    }

    @Test
    public void it_should_return_all_orders() {
        //This tests asserts both saving and then fetching of all records
        Order buyOrder = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.BUY).coinType("bit").userId("u1").build();
        Order sellOrder = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.SELL).coinType("bit").userId("u2").build();

        liveOrderBoard.addOrder(buyOrder);
        liveOrderBoard.addOrder(sellOrder);

        assertThat(liveOrderBoard.getOrders().size(), is(2));
        assertThat(liveOrderBoard.getOrders(), contains(buyOrder, sellOrder));
    }


    @Test
    public void it_should_be_able_cancel_order() {

        Order order = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.BUY).coinType("bit").userId("u1").build();
        //Given
        liveOrderBoard.addOrder(order);
        //When
        liveOrderBoard.deleteOrder(order.getId());
        //Then
        assertThat(liveOrderBoard.getOrders().size(), is(0));

    }

    @Test
    public void it_return_sell_order_summary_group_by_price_asc_order() {

        Order order1 = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.SELL).coinType("bit").userId("u1").build();
        Order order2 = Order.builder().pricePerCoin(BigDecimal.valueOf(3.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.SELL).coinType("lit").userId("u2").build();
        Order order3 = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.TEN).orderType(OrderType.SELL).coinType("lit").userId("u2").build();
        Order order4 = Order.builder().pricePerCoin(BigDecimal.valueOf(3.2D)).orderQuantity(BigDecimal.valueOf(9)).orderType(OrderType.SELL).coinType("lit").userId("u2").build();

        liveOrderBoard.addOrder(order1);
        liveOrderBoard.addOrder(order2);
        liveOrderBoard.addOrder(order3);
        liveOrderBoard.addOrder(order4);

        List<OrderSummary> orderSummary = liveOrderBoard.getSellOrderSummary();

        List<OrderSummary> expectedOrderSummary = new ArrayList();
        expectedOrderSummary.add(new OrderSummary(BigDecimal.valueOf(2.2D), BigDecimal.valueOf(11)));
        expectedOrderSummary.add(new OrderSummary(BigDecimal.valueOf(3.2D), BigDecimal.TEN));

        assertThat(orderSummary, is(expectedOrderSummary));

    }


    @Test
    public void it_return_buy_order_summary_group_by_price_desc_order() {

        Order order1 = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.BUY).coinType("bit").userId("u1").build();
        Order order2 = Order.builder().pricePerCoin(BigDecimal.valueOf(3.2D)).orderQuantity(BigDecimal.ONE).orderType(OrderType.BUY).coinType("lit").userId("u2").build();
        Order order3 = Order.builder().pricePerCoin(BigDecimal.valueOf(2.2D)).orderQuantity(BigDecimal.TEN).orderType(OrderType.BUY).coinType("lit").userId("u2").build();
        Order order4 = Order.builder().pricePerCoin(BigDecimal.valueOf(3.2D)).orderQuantity(BigDecimal.valueOf(9)).orderType(OrderType.BUY).coinType("lit").userId("u2").build();

        liveOrderBoard.addOrder(order1);
        liveOrderBoard.addOrder(order2);
        liveOrderBoard.addOrder(order3);
        liveOrderBoard.addOrder(order4);

        List<OrderSummary> orderSummary = liveOrderBoard.getBuyOrderSummary();

        List<OrderSummary> expectedOrderSummary = new ArrayList();
        expectedOrderSummary.add(new OrderSummary(BigDecimal.valueOf(3.2D), BigDecimal.TEN));
        expectedOrderSummary.add(new OrderSummary(BigDecimal.valueOf(2.2D), BigDecimal.valueOf(11)));

        assertThat(orderSummary, is(expectedOrderSummary));


    }
}