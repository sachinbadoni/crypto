package com.csfb.sb.dao;

import com.csfb.sb.domain.Order;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestStore {

    private Store<UUID, Order> orderStore;

    @Before
    public void init() {
        orderStore = new Store();
    }

    @Test
    public void it_should_be_able_to_store() {
        Order order = Order.builder().build();
        UUID uuid = UUID.randomUUID();
        orderStore.save(uuid, order);

        Optional<Order> fetchedOrder = orderStore.fetchByKey(uuid);
        assertTrue(fetchedOrder.isPresent());
        assertThat(fetchedOrder.get(), is(order));

    }

    @Test
    public void it_should_return_empty_optional_for_non_existing_order() {
        UUID randomId = UUID.randomUUID();
        Optional<Order> fetchedOrder = orderStore.fetchByKey(randomId);

        assertFalse(fetchedOrder.isPresent());
    }

    @Test
    public void it_should_return_empty_optional_for_null_key() {
        Optional<Order> fetchedOrder = orderStore.fetchByKey(null);
        assertFalse(fetchedOrder.isPresent());
    }

    @Test
    public void it_should_be_able_to_remove_value() {
        Order order = Order.builder().build();
        UUID uuid = UUID.randomUUID();
        orderStore.save(uuid, order);
        orderStore.remove(uuid);

        Optional<Order> fetchedOrder = orderStore.fetchByKey(uuid);
        assertFalse(fetchedOrder.isPresent());
    }

    @Test
    public void it_should_be_able_to_handle_non_existing_key_removal() {
        UUID uuid = UUID.randomUUID();
        orderStore.remove(uuid);

        Optional<Order> fetchedOrder = orderStore.fetchByKey(uuid);
        assertFalse(fetchedOrder.isPresent());
    }

    @Test
    public void it_should_be_able_to_return_all_records() {
        orderStore.save(UUID.randomUUID(), Order.builder().build());
        orderStore.save(UUID.randomUUID(), Order.builder().build());

       assertThat(orderStore.getAllOrders().size(), is(2));
    }

}
