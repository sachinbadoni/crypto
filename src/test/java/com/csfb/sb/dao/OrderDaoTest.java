package com.csfb.sb.dao;

import com.csfb.sb.domain.Order;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.csfb.sb.domain.OrderType.BUY;
import static com.csfb.sb.domain.OrderType.SELL;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class OrderDaoTest {

    private OrderDao orderDao;

    @Before
    public void init() {
        orderDao = new OrderDao();
    }

    @Test
    public void it_should_store_order() {
        Order order = defaultOrder();
        Order saveOrder = orderDao.saveOrder(order);
        assertNotNull(saveOrder.getId());

    }

    @Test
    public void it_should_be_able_to_fetch_order() {
        Order order = defaultOrder();
        Order saveOrder = orderDao.saveOrder(order);
        assertNotNull(saveOrder.getId());

        Optional<Order> optionalOrder = orderDao.getOrder(saveOrder.getId());
        assertTrue(optionalOrder.isPresent());
        assertThat(optionalOrder.get(), is(saveOrder));

    }

    @Test
    public void it_should_be_able_return_immutable_order() {
        Order order = defaultOrder();
        Order saveOrder = orderDao.saveOrder(order);
        assertNotNull(saveOrder.getId());
        assertNotNull(order.getId());

        Optional<Order> optionalOrder = orderDao.getOrder(saveOrder.getId());
        assertTrue(optionalOrder.isPresent());
        assertThat(optionalOrder.get(), is(saveOrder));

        assertFalse(order == saveOrder);
        assertFalse(saveOrder == optionalOrder.get());
    }

    @Test
    public void it_should_be_able_to_cancel_order() {
        Order order = defaultOrder();
        Order saveOrder = orderDao.saveOrder(order);
        assertNotNull(saveOrder.getId());

        orderDao.cancelOrder(saveOrder.getId());

        Optional<Order> optionalOrder = orderDao.getOrder(saveOrder.getId());
        assertFalse(optionalOrder.isPresent());

    }

    @Test
    public void it_should_be_able_to_fetch_all_orders() {
        Order order1 = Order.builder().orderQuantity(ONE).userId("user-1").coinType("(Litecoin").orderType(BUY).build();
        Order order2 = Order.builder().orderQuantity(ONE).userId("user-2").coinType("(Bitcoin").orderType(SELL).build();

        orderDao.saveOrder(order1);
        orderDao.saveOrder(order2);

        List<Order> orders = orderDao.getAllOrders();
        assertThat(orders, is(Arrays.asList(order1, order2)));
    }

    private Order defaultOrder() {
        Order build = Order.builder()
                .orderQuantity(ONE)
                .userId("user-1")
                .coinType("(Litecoin")
                .orderType(BUY)
                .pricePerCoin(TEN)
                .build();
        return build;
    }
}