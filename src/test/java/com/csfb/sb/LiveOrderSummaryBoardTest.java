package com.csfb.sb;

import com.csfb.sb.domain.Order;
import com.csfb.sb.domain.OrderType;
import com.csfb.sb.manager.LiverOrderSummaryManager;
import com.csfb.sb.services.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LiveOrderSummaryBoardTest {

    @Mock
    LiverOrderSummaryManager mockLiveOrderSummaryManager;

    @Mock
    OrderService mockOrderService;

    @InjectMocks
    LiveOrderSummaryBoard liveOrderSummaryBoard;

    public void init(){
        reset(mockOrderService, mockLiveOrderSummaryManager);
    }

    @Test
    public void add_order_should_call_order_service() {
        liveOrderSummaryBoard.addOrder(null);

        verify(mockOrderService).saveOrder(any(Order.class));
    }


    @Test
    public void delete_order_should_call_order_service() {
        liveOrderSummaryBoard.deleteOrder(null);

        verify(mockOrderService).cancelOrder(any(UUID.class));
    }

    @Test
    public void get_all_orders_should_call_order_service() {
        liveOrderSummaryBoard.getOrders();

        verify(mockOrderService).getAllOrders();
    }


    @Test
    public void get_buy_summary_should_call_order_service_and_order_manager() {
        liveOrderSummaryBoard.getBuyOrderSummary();

        verify(mockOrderService).getAllOrders();
        verify(mockLiveOrderSummaryManager).generateOrderSummary(eq(OrderType.BUY), anyList());
    }

    @Test
    public void get_sell_summary_should_call_order_service_and_order_manager() {
        liveOrderSummaryBoard.getSellOrderSummary();

        verify(mockOrderService).getAllOrders();
        verify(mockLiveOrderSummaryManager).generateOrderSummary(eq(OrderType.SELL), anyList());
    }

}