package com.csfb.sb.manager;

import com.csfb.sb.domain.Order;
import com.csfb.sb.domain.OrderSummary;
import com.csfb.sb.domain.OrderType;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class OrderManagerTest {

    private LiverOrderSummaryManager orderManager;

    @Before
    public void setUp() {
        orderManager = new LiverOrderSummaryManager();
    }

    @Test
    public void it_should_group_orders_with_sell_type_order_by_price(){

        List<Order> orders = new ArrayList();
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(350.1D)).orderQuantity(BigDecimal.valueOf(2L)).coinType("Lit").userId("user-1").orderType(OrderType.SELL).build());
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(450.1D)).orderQuantity(BigDecimal.valueOf(3L)).coinType("bitCoin").userId("user-1").orderType(OrderType.SELL).build());
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(350.1D)).orderQuantity(BigDecimal.valueOf(4L)).coinType("Lit").userId("user-1").orderType(OrderType.SELL).build());
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(450.1D)).orderQuantity(BigDecimal.valueOf(5L)).coinType("Lit").userId("user-1").orderType(OrderType.SELL).build());

        List<OrderSummary> orderSummaries = orderManager.generateOrderSummary(OrderType.SELL, orders);

        assertThat(orderSummaries.size(), is(2));

        OrderSummary expectedSummary1 = new OrderSummary(BigDecimal.valueOf(350.1D), BigDecimal.valueOf(6L));
        OrderSummary expectedSummary2 = new OrderSummary(BigDecimal.valueOf(450.1D), BigDecimal.valueOf(8L));

        assertThat(orderSummaries, contains(expectedSummary1, expectedSummary2));

    }


    @Test
    public void it_should_group_orders_with_buy_type_order_by_price_reverse_order(){

        List<Order> orders = new ArrayList();
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(350.1D)).orderQuantity(BigDecimal.valueOf(2L)).coinType("Lit").userId("user-1").orderType(OrderType.BUY).build());
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(450.1D)).orderQuantity(BigDecimal.valueOf(3L)).coinType("bitCoin").userId("user-1").orderType(OrderType.BUY).build());
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(350.1D)).orderQuantity(BigDecimal.valueOf(4L)).coinType("Lit").userId("user-1").orderType(OrderType.BUY).build());
        orders.add(Order.builder().pricePerCoin(BigDecimal.valueOf(450.1D)).orderQuantity(BigDecimal.valueOf(5L)).coinType("Lit").userId("user-1").orderType(OrderType.BUY).build());

        List<OrderSummary> orderSummaries = orderManager.generateOrderSummary(OrderType.BUY, orders);

        assertThat(orderSummaries.size(), is(2));

        OrderSummary expectedSummary1 = new OrderSummary(BigDecimal.valueOf(450.1D), BigDecimal.valueOf(8L));
        OrderSummary expectedSummary2 = new OrderSummary(BigDecimal.valueOf(350.1D), BigDecimal.valueOf(6L));

        assertThat(orderSummaries, contains(expectedSummary1, expectedSummary2));

    }

}