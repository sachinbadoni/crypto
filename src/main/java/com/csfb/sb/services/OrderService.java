package com.csfb.sb.services;

import com.csfb.sb.dao.OrderDao;
import com.csfb.sb.domain.Order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service layer to orchestrate calls to DAOs
 */
public class OrderService {
    private OrderDao orderDao;

    public OrderService() {
        this.orderDao = new OrderDao();
    }

    public Order saveOrder(Order order) {
        return orderDao.saveOrder(order);
    }

    public Optional<Order> getOrder(UUID orderKey) {
        return orderDao.getOrder(orderKey);
    }

    public void cancelOrder(UUID orderKey) {
        orderDao.cancelOrder(orderKey);
    }

    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }


}
