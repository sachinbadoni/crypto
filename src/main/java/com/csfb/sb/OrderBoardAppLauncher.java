package com.csfb.sb;

import com.csfb.sb.domain.Order;
import com.csfb.sb.domain.OrderType;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.UUID;

/**
 * CLI for running this App standalone.
 * <p>
 * This is just a basic CLI, there is not error handling
 */
public class OrderBoardAppLauncher {
    private static String CMD_OPTIONS = "\nA to add an order " +
            "\nD to delete and order " +
            "\nL to list orders " +
            "\nS for order summary " +
            "\nQ to quit\n";

    public static void main(String[] args) {
        LiveOrderSummaryBoard liveOrderSummaryBoard = new LiveOrderSummaryBoard();

        String command;
        boolean run = true;
        while (run) {
            System.out.println(CMD_OPTIONS);
            Scanner scanner = new Scanner(System.in);
            command = scanner.nextLine();
            switch (command.toLowerCase()) {

                case "a":
                    System.out.println("Please provide order type. e.g. BUY or SELL");
                    String orderType = scanner.nextLine();
                    System.out.println("Please provide order quantity, can be any number\n");
                    String orderQuantity = scanner.nextLine();

                    System.out.println("Please provide price per coin. \n");

                    String price = scanner.nextLine();

                    System.out.println("Please provide coinType. \n");
                    String coinType = scanner.nextLine();

                    System.out.println("Please provide userName. \n");
                    String userName = scanner.nextLine();


                    Order order = Order.builder().orderType(OrderType.valueOf(orderType))
                            .orderQuantity(new BigDecimal(orderQuantity))
                            .userId(userName).coinType(coinType).pricePerCoin(new BigDecimal(price)).build();

                    liveOrderSummaryBoard.addOrder(order);
                    System.out.println("Order is added...");
                    break;

                case "d":
                    System.out.println("Please provide order id to delete");
                    String orderId = scanner.nextLine();
                    liveOrderSummaryBoard.deleteOrder(UUID.fromString(orderId));
                    System.out.println("Order is deleted...");
                    break;
                case "l":
                    System.out.println(liveOrderSummaryBoard.getOrders());
                    break;
                case "q":
                    run = false;
                    break;
                case "s":
                    System.out.println("Which summary you want e. e.g. BUY or SELL");
                    orderType = scanner.nextLine();
                    if (orderType.equalsIgnoreCase("SELL")) {
                        System.out.println(liveOrderSummaryBoard.getSellOrderSummary());
                    } else if (orderType.equalsIgnoreCase("BUY")) {
                        System.out.println(liveOrderSummaryBoard.getBuyOrderSummary());
                    }
                    break;
                default:
                    System.out.printf("Didn't understand command");
                    break;
            }
        }
    }

}
