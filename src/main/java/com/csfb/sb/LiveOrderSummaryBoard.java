package com.csfb.sb;

import com.csfb.sb.domain.Order;
import com.csfb.sb.domain.OrderSummary;
import com.csfb.sb.domain.OrderType;
import com.csfb.sb.manager.LiverOrderSummaryManager;
import com.csfb.sb.services.OrderService;

import java.util.List;
import java.util.UUID;

public class LiveOrderSummaryBoard {
    private LiverOrderSummaryManager liverOrderSummaryManager;
    private OrderService orderService;

    public LiveOrderSummaryBoard() {
        liverOrderSummaryManager = new LiverOrderSummaryManager();
        orderService = new OrderService();
    }

    public void addOrder(Order order) {
        orderService.saveOrder(order);
    }

    public void deleteOrder(UUID uuid) {
        orderService.cancelOrder(uuid);
    }

    public List<Order> getOrders() {
        return orderService.getAllOrders();
    }

    public List<OrderSummary> getSellOrderSummary() {
        return liverOrderSummaryManager.generateOrderSummary(OrderType.SELL, orderService.getAllOrders());
    }

    public List<OrderSummary> getBuyOrderSummary() {
        return liverOrderSummaryManager.generateOrderSummary(OrderType.BUY, orderService.getAllOrders());
    }

}
