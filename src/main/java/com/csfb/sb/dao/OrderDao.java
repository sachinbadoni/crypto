package com.csfb.sb.dao;

import com.csfb.sb.domain.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public class OrderDao {

    private Store<UUID, Order> orderStore;

    public OrderDao() {
        orderStore = new Store();
    }

    public Order saveOrder(Order order) {
        UUID orderId = UUID.randomUUID();
        order.setId(orderId);
        orderStore.save(orderId, order);
        return (Order) order.clone();
    }


    public Optional<Order> getOrder(UUID orderId) {
       return orderStore.fetchByKey(orderId);
    }

    public void cancelOrder(UUID orderId) {
         orderStore.remove(orderId);
    }

    public List<Order> getAllOrders() {
        return new ArrayList<>(orderStore.getAllOrders().values());
    }
}
