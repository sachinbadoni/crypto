package com.csfb.sb.dao;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * InMemory store
 */
public class Store<K, V> {
    private Map<K, V> orderMap = new LinkedHashMap<>();

    public void save(K key, V value) {
        orderMap.put(key,value);
    }

    public Optional<V> fetchByKey(K key) {
        return Optional.ofNullable(orderMap.get(key));
    }

    public void remove(K key){
        orderMap.remove(key);
    }

    public Map<K, V> getAllOrders() {
        return orderMap;
    }
}
