package com.csfb.sb.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

import static java.lang.String.format;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
public class Order implements Cloneable {
    //id for internal reference
    @Setter
    private UUID id;
    private String userId;
    private String coinType;
    private BigDecimal orderQuantity;
    private OrderType orderType;
    //Assumption is made that all prices are
    private BigDecimal pricePerCoin;

    @Override public Object clone()  {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(" Can't clone");
        }
    }

    @Override public String toString() {
        return format("id=%s user=%s, coinType=%s, orderQuantity=%s, orderType=%s, pricePerCoin=%s",
                id, userId, coinType, orderQuantity, orderType, pricePerCoin);

    }
}
