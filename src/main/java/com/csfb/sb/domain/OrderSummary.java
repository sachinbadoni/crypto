package com.csfb.sb.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

import static java.lang.String.format;


@AllArgsConstructor
@Data
public class OrderSummary {

    private BigDecimal price;
    private BigDecimal quantity;

    @Override
    public String toString() {
        return format("\n%s for £%s\n", quantity, price);
    }
}
