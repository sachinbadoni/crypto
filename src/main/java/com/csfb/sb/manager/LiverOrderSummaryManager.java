package com.csfb.sb.manager;

import com.csfb.sb.domain.Order;
import com.csfb.sb.domain.OrderSummary;
import com.csfb.sb.domain.OrderType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class LiverOrderSummaryManager {

    public List<OrderSummary> generateOrderSummary(OrderType orderType, List<Order> orders) {

        List<OrderSummary> orderSummaries = groupOrderByPrice(orderType, orders);

        if (orderType.equals(OrderType.BUY)) {
            Collections.reverse(orderSummaries);
        }
        return orderSummaries;
    }

    /**
     * Grouping order by Price
     */
    private List<OrderSummary> groupOrderByPrice(OrderType orderType, List<Order> orders) {

        // Treemap will provide ordering based on key
        Map<BigDecimal, OrderSummary> orderSummariesMap = new TreeMap<>();

        filterOrdersByType(orderType, orders).stream().forEach(order -> {
            OrderSummary orderSummary = new OrderSummary(order.getPricePerCoin(), order.getOrderQuantity());
            OrderSummary oldSummaryValue = orderSummariesMap.put(order.getPricePerCoin(), orderSummary);

            if (oldSummaryValue != null) {
                addQuantity(orderSummary, oldSummaryValue);
            }
        });

        return new ArrayList<>(orderSummariesMap.values());
    }

    /**
     * Filter orders by Order Type
     */
    private List<Order> filterOrdersByType(OrderType orderType, List<Order> orders) {
        return orders.stream().filter(o -> o.getOrderType().equals(orderType)).collect(Collectors.toList());
    }

    /**
     * Adding old and new order's quantity
     */
    private void addQuantity(OrderSummary orderSummary, OrderSummary oldSummaryValue) {
        BigDecimal totalQuantity = orderSummary.getQuantity().add(oldSummaryValue.getQuantity());
        orderSummary.setQuantity(totalQuantity);
    }

}
